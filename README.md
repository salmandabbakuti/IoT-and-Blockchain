# IoT-and-Blockchain

follow this medium article https://link.medium.com/J78zqfMHwW

# Pre-Requisites

Python 2.7 + Python
https://www.python.org/downloads/

Git
https://git-scm.com/downloads

### Npm packages
Truffle

```
npm install -g truffle
```

Pip

```
npm i pip
```

ganache-cli

```
npm install -g ganache-cli
```

### Pip packages

Flask

Web3.py 

GPIO Emulator

```
pip install requirements.txt
```

# STEPS

>***Make Sure prerequisites are installed***

1.clone the repository into desktop

```
git clone https://github.com/Salmandabbakuti/IoT-and-Blockchain.git 
```


2. install required PiP dependencies

```
pip install requirements.txt
```

3.instantiate Ganache-cli Private Network.

```
ganache-cli
```

4.run app

```
py app.py
```

wait a moment. this will take a minute or two.

After deployment of contract in private network, you will see status of each pin in your command line and Emulator on Screen.

5.head into browser and type ```<Your computer ip>/``` and control through UI.

<img align=center src="https://github.com/Salmandabbakuti/IoT-and-Blockchain/blob/master/Screenshot%20(81).png">

basically, it will write particular pin status on blockchain and retrieve it from the chain. and then activate pin confiuaration accordingly.

boom.

 © Salman Dabbakuti
